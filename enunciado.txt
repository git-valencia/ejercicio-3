1. Copia los dos directorios en un nuevo directorio fuera de éste.
2. En el nuevo directorio, crea un repositorio Git.
3. Añade al índice todos los archivos y crea un commit inicial con todos ellos.
4. Abre el archivo palindromos/palindromos.txt y añade una nueva línea con la siguiente frase:
"Anita lava la tina.". Guarda el archivo.
5. Abre el archivo quijote/quijote.txt y sustituye los puntos suspensivos por esta frase:
", no corre el mar sino vuela un velero bergantín.". Guarda el archivo.
6. Añade los cambios al índice y haz un commit con ellos.
7. Abre el archivo quijote/quijote.txt y corrige la anterior frase del punto 5 por ésta:
", no ha mucho tiempo que vivía un hidalgo de los de lanza en astillero.". Guarda el archivo.
8. Abre el archivo palindromos/palindromos.txt y añade una nueva línea con la siguiente frase:
"Sé verlas al revés.". Guarda el archivo.
9. Añade únicamente la corrección del punto 7 al último commit que has hecho.
10. Crea un nuevo commit con el cambio del punto 8.
11. Comprueba que te ha quedado un log con tres commits.
12. Comprueba que en el penúltimo commit has corregido bien la frase del Quijote.
